# Reaction-Diffusion Pythons Florida

# Reaction-Diffusion Pythons Florida

This is the Matlab code associated to the article *Spatially explicit control of invasive species using a reaction–diffusion model*, available [online](https://www.sciencedirect.com/science/article/pii/S0304380016301934?casa_token=nbjmDvmTahAAAAAA:faJezC2GFrQtiHcdrq3gl7owg6KCgGajWP9reG5yrYORY6oLh2VN0F-P98TfbltcMioZa-Ydaw), with the full citation: 

>Bonneau, M., Johnson, F. A., & Romagosa, C. M. (2016). Spatially explicit control of invasive species using a reaction–diffusion model. Ecological modelling, 337, 15-24.

This depository is divided into two folders, *Example-Prediction-With-Control* and *Prediction-Without-Control*. 

We advise to start by opening the first repositiory *Example-Prediction-With-Control* as far as a full example is provided.
You'll see how to setup all the necessary variables in order to use the code for your own application. 

The second directory is specific for making prediction in Florida, using the main fucntion. 
Predictions have already been computed using the main function and are available in `predictions.zip` archive. 
All the predictions are saved under the `xlsx` format, the first two columns being for the spatial coordinates and the third column for the python density value. 
There is one file per year, and we saved two files, one using the UTM coordinates, zone 17N, and the other using the latitude-longitute coordinates. 

The model parameters are the one described in the article (see `Table 1`):
- Growth rate: 0.468.
- Diffusion coefficient: 16.2 km2/year
- Invasion started in 1995 in Flamingo `(508790 E,2821227 N) (UTM coordinates, zone 17, northern hemisphere)`
- Carrying capacity of 100 pythons per square kilometer.
- Spatial resolution of the predictions: 1 square kilometer. 

Enjoy and thank you to cite this work when using this repository. 
