function DrawDensity(n,Data,nf)
% Draw the density in each cell
if nargin == 2
    nf = 1;
end

figure(nf)
clf
hold on

C = ones-n./Data.Kcapacity; % Will be used to set the color
C(C<0) = zeros;
C(C>1)=1;

for i = 1:size(Data.Grid,1)
    
    if Data.ZNumber(i) == 0 % We use color blue for the cells outside of the domain
        p=rectangle('Position',Data.Grid(i,:),'FaceColor',[0 , 0.7 , 1]);
        set(p,'edgecolor',[0 , 0.7 , 1])
        
    else
        p=rectangle('Position',Data.Grid(i,:),'FaceColor',[C(i) , C(i) , C(i)]); % We use gray for density. It is black when it reach K
        set(p,'edgecolor',[C(i) , C(i) , C(i)])
        
    end
end

%% The legend
p1=plot(nan,nan,'s','markeredgecolor',[0 0 0],...
    'markerfacecolor',[1 1 1]);
p2=plot(nan,nan,'s','markeredgecolor',[.5 .5 .5],...
    'markerfacecolor',[.5 .5 .5]);
p3=plot(nan,nan,'s','markeredgecolor',[0 0 0],...
    'markerfacecolor',[0 0 0]);
legend([p1 p2 p3],{'0 Individuals/km^2';'K/2 Individuals/km^2';'K Individuals/km^2'},'Location','SouthOutside')

axis equal
axis off
