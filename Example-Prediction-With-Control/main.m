% This file should be used to setup the problem and run the simulation.

%% First create a structure, called Data, where all the information are
%% stored. 
%% Here we used arbitrarly values

%% Add the field GrowthRate = \epsilon
Data = struct('GrowthRate',0.3); 

%% Add the Diffusion coefficient field, Diff_Coef = D
Data = setfield(Data,'Diff_Coef',10); 

%% Add the carrying capacity field, Kcapacity = K
Data = setfield(Data,'Kcapacity',20); 

%% Add the yearly movement rate field, rm = r_m
Data = setfield(Data,'rm',10);

%% Add the initial value of the state of the population field, Init = n^0.
%% Here we use a square study area, where the individuals are first located on the bottom left corner. The initial population here is equal to the carrying
%% capacity.
rows = 100; % The number of cells on the North-South dimension
column = 120; % The number of cells on the East-West dimension
Init = zeros(rows,column);
Init(end-5,6) = Data.Kcapacity; % It's where the population starts
Data = setfield(Data,'Init',Init);

%% Add the zone number field. 
%% The zone number is created to differenciate the zones that are inside/outside the domain and/or the different control area(s).

%% We first create the domain with triangular eastern and wesern frontiers.
E = 5 + randi(5,1);
W = 5 + randi(5,1);
East = ones(size(Data.Init,1),1);         % East will contain the column number of each cell on the eastern frontier. e.g. East(3)=5 -> cell on row 3, column 1 is on the estern frontier and thus cell on row 3 column 4 (resp. 6) is outside (resp. inside) the domain
West = column.*ones(size(Data.Init,1),1); % Same as East but for the western boundary

East(1:E) = 1:E;
East((E+1):(2*E)) = E:-1:1;

West(1:W) = column:-1:(column-W+1);
West((W+1):(2*W)) = (column-W+1):1:column;
West = rot90(rot90(West));


ZNumber = -ones(rows,column); % The zone numbers are initialize to -1 all over the domain. Then, this zone number will be used for the cells that are located outside the domain.

%% Then we initialize to one the zone number of the cells that are inside the random domain
for i = 1:rows
   
    ZNumber(i,East(i):West(i)) = ones;
end

%% Then we define the different zones where control will be applied. 
%% Here we considered two different control actions, so we have to define two different zones.

%% The first zone is composed of two square areas of size 10, with a center located randomly.

Center_Zone_X = randi(50,[1 , 2]) + 15.*ones;
Center_Zone_Y(1) = randi(15) + 15.*ones;
Center_Zone_Y(2) = randi(25) + 35.*ones;

%% We set the zone number to 2, for the first square of the first control zone
% First square
for i = (Center_Zone_X(1)-5):(Center_Zone_X(1)+4)
    
    ZNumber(sub2ind([rows , column],Center_Zone_Y(1)-5):(Center_Zone_Y(1)+4),repmat(i,[1 , 10])) = 2;
end

% Second square
for i = (Center_Zone_X(2)-5):(Center_Zone_X(2)+4)
    
    ZNumber(sub2ind([rows , column],Center_Zone_Y(2)-5):(Center_Zone_Y(2)+4),repmat(i,[1 , 10])) = 2;
end

%% The second zone is a fixed rectangular zone C_{T_2} = [25,50]x[10,30] (with origin of the coordinate on the bottom left corner)
for i = 25:50
    
    ZNumber(sub2ind([rows , column],70:90,repmat(i,[1 , 21]))) = 3;
end

%% We draw the domain
figure(1)
clf(1)
imagesc(ZNumber)
colormap summer
colorbar
axis image
title('The domain and the spatial repartition of the different removal actions.','FontSize',30)
axis off

%% We transform ZNumber into a vector using the reshape function
ZNumber = reshape(ZNumber,[numel(ZNumber) , 1]); 

%% We add the ZNumber field
Data = setfield(Data,'ZNumber',ZNumber);

%% We define a matrix that store the spatial coordinate of the center of each cell.
deltaX = 2;
deltaY = 2; % We first define the size of the grid, here we use 2km
Data = setfield(Data,'deltaX',deltaX);
Data = setfield(Data,'deltaY',deltaY); % We save the value of the spatial step

% Here the cell locate on last row and first column has coordinate
% (deltaX/2,deltaY/2)
CoordX = zeros(rows,column);
CoordY = zeros(rows,column);

X = (deltaX/2):deltaX:((deltaX/2)+(column-1)*deltaX);
Y = rot90((deltaY/2):deltaY:((deltaY/2)+(rows-1)*deltaY));

CoordX = repmat(X,[rows , 1]);   % CoordX(i,j) = x_{i,j}, contains the X-coordinate of the cell center located row i column j
CoordY = repmat(Y,[1 , column]); % CoordY(i,j) = y_{i,j}, contains the Y-coordinate of the cell center located row i column j

%% We transform CoordX and CoordY into a vector
CoordX = reshape(CoordX,[numel(CoordX) , 1]);
CoordY = reshape(CoordY,[numel(CoordY) , 1]);

%% We store these coordinates into a new field
Data = setfield(Data,'CoordC',[CoordX , CoordY]);

%% We create a field grid to be used with the function rectangle which will draw the population density
Grid = [(Data.CoordC(:,1)-deltaX.*ones) , (Data.CoordC(:,2)-deltaY.*ones) , deltaX.*ones(rows*column,1) , deltaY.*ones(rows*column,1)];
Data = setfield(Data,'Grid',Grid);



%% We define and construct the boundary conditions. 
%% Here we use Dirichlet boundary conditions for the Eastern and Western borders (with b_D = 0). 
%% And we use Neuman boundary condition for the Northern and Soutern borders (with b_N = 0).
%% We create a matrix Boundary, with values:
%%  - 0 if the cell is not on the boundaries of the domain
%%  - 1 if the cell is on the easter or western boundary (Dirichlet).
%%  - 2 if the cell is on the northern or southern boundary (Neuman).

B = zeros(rows,column);

% Dirichlet
B(sub2ind([rows,column],1:rows,East')) = ones;
B(sub2ind([rows,column],1:rows,West')) = ones;

% Neumann
B(1,(East(1)+1):(West(1)-1)) = 2;
B(end,(East(end)+1):(West(end)-1)) = 2;

%% Then we store the indice of each of the cell on the boundary for each condition
Data = setfield(Data,'Dirichlet',find(B == 1));
Data = setfield(Data,'Neumann',find(B == 2));

%% We now have to compute the indices of the nearest neighbors of the cells on the Neumann boundaries that are located inside the domain.
I = find(B == 2);
[r , c] = ind2sub([rows , column],I);
Neighbors_Neumann = zeros(size(I));

for i = 1:length(I)
    if r(i) == 1
        
        if (c(i) <= East(2))
            
            Neighbors_Neumann(i) = sub2ind([rows , column],2,East(2));
        else
            
            Neighbors_Neumann(i) = sub2ind([rows , column],2,c(i));
        end
        
    else
        
        if (c(i) >= West(end-1))
            
            Neighbors_Neumann(i) = sub2ind([rows , column],rows-1,West(end-1));
        else
            
            Neighbors_Neumann(i) = sub2ind([rows , column],rows-1,c(i));
        end
    end
end

%% Then we create a new field, which stores the indice of the cells that are in the normal direction of each cell that are on the north or south boundary
Data = setfield(Data,'Neighbors_Neumann',Neighbors_Neumann);



%% The Data structure is ready to be used.
%% Next step is to compute the value of r_{T_i}, \forall i and to set up the removal rates \alpha_{T_i}

%% r_{T_i}
NbAction = sum(unique(Data.ZNumber)>1);
I_Action = cell(1,NbAction);

for i = 1:NbAction
   
    I_Action{i} = find(Data.ZNumber == (1+i));
end

rT = zeros(rows*column,NbAction);
J = [];
for i = 1:(rows*column)
    
    d = sqrt((Data.CoordC(:,1)-Data.CoordC(i,1).*ones).^2+(Data.CoordC(:,2)-Data.CoordC(i,2).*ones).^2);
    I = find(d<=Data.rm);
    I = setdiff(I,i);
    I = I(Data.ZNumber(I)~=0); % Cells located outside of the domain are not possible neighbors.
    
    if isempty(I)
        
        rT(i,:) = 0;
    else
        
        for n = 1:NbAction
        
            IF = intersect(I,I_Action{n});
            rT(i,n) = length(IF)/length(I);
        end
    end
end

rT = reshape(rT,[rows , column , NbAction]);
for i = 1:NbAction
    
    figure(1 + i)
    imagesc(squeeze(rT(:,:,i)))
    colormap summer
    colorbar
    axis image
    title(['Value of r_{T_' int2str(i) '}'],'FontSize',30)
    axis off
end


%% We now define the efficacy of each action. We store it inot the vector Efficiency. 
%% Where, Efficacy(i) = \alpha_{T_i}.
Efficacy = [0.1 , 0.5]; % Arbitrarly values.


%% We finally create the matrix Hr which contains the removal rate of each cell.
%% \forall (x_i,y_j)\in\Omega, Hr(i,j) = \beta((x_i,y_j)). 
%% Thus Hr(i,j) is 0 if no control action is used in the cell located row i, column j.
%% Hr(i,j) is \alpha_{T_k} if control action number k is used in the cell located row i, column j.

Hr = zeros(length(Data.CoordC),1);
for i = 1:NbAction
    
    Hr(I_Action{i}) = Efficacy(i);
end
Hr = reshape(Hr,[rows , column]);
figure(1 + NbAction +1)
imagesc(Hr)
colormap summer
colorbar
axis image
title('Value of \beta(x)','FontSize',30)
axis off




% %% We now run the numerical method which computes the density in each cell
T = 100; % This is the time horizon
Density = Modified_RD(T,Data,Efficacy,rT,Hr);


% We can draw the density at the end of the time horizon in figure 10
% DrawDensity(squeeze(Density(:,:,100)),Data,10)
% figure(10);hold on; title('Density after 100 years')

% We can draw the density after 20 years in figure 11
% DrawDensity(squeeze(Density(:,:,20)),Data,11)

% We can draw the density after 50 years in figure 12
DrawDensity(squeeze(Density(:,:,50)),Data,12)
figure(12);hold on; title('Density after 50 years')

% We can draw the density after 70 years in figure 13
% DrawDensity(squeeze(Density(:,:,70)),Data,13)
% figure(13);hold on; title('Density after 70 years')


%% We can compute the density without any control, just fix Hr to zeros and pT to zeros
Density_NT = Modified_RD(T,Data,0,zeros(rows,column,1),zeros(rows,column));

% And then draw the density after 50 years in figure 14
DrawDensity(squeeze(Density_NT(:,:,50)),Data,14)
figure(14);hold on; title('Density after 50 years - Without control')


%% We can compute the density with the CEH model, just fix pT to zeros
Density_CEH = Modified_RD(T,Data,Efficacy,zeros(rows,column,2),Hr);

% And then draw the density after 50 years in figure 15
DrawDensity(squeeze(Density_CEH(:,:,50)),Data,15)
figure(12);hold on; title('Density after 50 years - CEH model')
