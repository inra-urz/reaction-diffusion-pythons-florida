function Res = Modified_RD(T,Data,Efficacy,rT,Hr)



Res = zeros(size(Data.Init,1) , size(Data.Init,2),T); % Will stroe the matrix density at each time step
D = Data.Init; % D is the current density at time step t. n^{k}_{i,j}

%%  We compute the deltat which respect \frac{D\Delta t}{\Delta x^2},\frac{D\Delta t}{\Delta y^2}<1.
%% If deltaX and deltaY are too small, which will slow the program, the function return an error message
deltaT = min((Data.deltaX^2)/Data.Diff_Coef,(Data.deltaX^2)/Data.Diff_Coef);
eps = [0.1 , 0.01 , 0.001 , 0.0001];
k = 1;
while (k <= length(eps))
    
    if (deltaT > eps(k))
        deltaT = eps(k);
        break
    end
    k = k + 1;
end

if (k == (length(eps) + 1))
    disp('Please choose a higher deltaX and deltaY.')
    return
end


eps = 0.1;
NbMax = 500;

DN = D; % DN is the density at time step t + deltaT

itr = 1;
Tsum = sum(repmat(reshape(Efficacy',[1 , 1 , length(Efficacy)]),[size(rT,1) , size(rT,2) , 1]).*rT,3);
DiffCoef = Data.Diff_Coef./2;

%% We apply the Crank-Nicholson algorithm
for year = 1:T
    
    for t = 1:(1/deltaT)
        
        errorD = 100;
        itr = 1;
        while (errorD > eps) && (itr < NbMax)
            
            itr = itr + 1;
            %% k
            Dip1 = [D(:,2:end) , zeros(size(D,1),1)];      % n^{i+1,j}_{k}
            Dim1 = [zeros(size(D,1),1) , D(:,1:(end-1))];  % n^{i-1,j}_{k}
            
            Djp1 = [zeros(1,size(D,2)) ; D(1:(end-1),:)];  % n^{i,j+1}_{k}
            Djm1 = [D(2:end,:) ; zeros(1,size(D,2))];      % n^{i,j-1}_{k}
            
            B = D; % n^k_{i,j}
            
            B = B + (deltaT/(Data.deltaX^2)).*(DiffCoef.*( Dip1 - 2.*D + Dim1 ));
            
            B = B + (deltaT/(Data.deltaY^2)).*(DiffCoef.*( Djp1 - 2.*D + Djm1 ));
            
            Growth = Data.GrowthRate.*(ones - D./Data.Kcapacity);
            
            Growth = Growth - Tsum - Hr;
            Growth(Growth<0) = zeros;
            
            B = B + (1/2).*(deltaT.*(D.*Growth));
            
            %% k + deltaT
            Dip1 = [DN(:,2:end) , zeros(size(D,1),1)];      % n^{i+1,j}_{k+deltaT}
            Dim1 = [zeros(size(D,1),1) , DN(:,1:(end-1))];  % n^{i-1,j}_{k+deltaT}
            
            Djp1 = [zeros(1,size(D,2)) ; DN(1:(end-1),:)];  % n^{i,j+1}_{k+deltaT}
            Djm1 = [DN(2:end,:) ; zeros(1,size(D,2))];      % n^{i,j-1}_{k+deltaT}
            
            BN = (deltaT/(Data.deltaX^2)).*(DiffCoef.*( Dip1 - 2.*DN + Dim1 ));
            
            BN = BN + (deltaT/(Data.deltaX^2)).*(DiffCoef.*( Djp1 - 2.*DN + Djm1 ));
            
            Growth = Data.GrowthRate.*(ones - DN./Data.Kcapacity);
            
            Growth = Growth - Tsum - Hr;
            Growth(Growth<0) = zeros;
            
            
            BN = BN + (1/2).*(deltaT.*(DN.*Growth));
            
            
            
            DN = B + BN;
            
            %% We set to 0 the cell outside of the domain
            DN(Data.ZNumber == 0) = zeros;
            
            %% Dirichlet boundary condition 
            DN(Data.Dirichlet) = zeros;
            
            %% Neumann boundary condition
            DN(Data.Neumann) = DN(Data.Neighbors_Neumann);
            
            %% We use a Jacobi's method to test convergence
            errorD = norm(D-DN,inf)/norm(DN,inf); 
            
        end
        D = DN; % n^{k+1}
        D(D>Data.Kcapacity) = Data.Kcapacity;
    end
    Res(:,:,year) = D;
end