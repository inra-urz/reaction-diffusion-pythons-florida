function Res = Logistic_Density(T,Data,Efficacy,pT,Hr,IG,IG2,IL,IL2,IN)
%% This is the main function to run the prediciton 
%% T is the time horizon
%% Data is the initial data structure
%% Efficacy is the efficacy of the control methods. Use zeos if prediction with no control. It is a vector, one efficacy per control method
%% pT is the decrease in diffusion due to control on neigborood locations (i.e. removal rate increases when pythons are mooving because it's quite the only moment they are visible). It noted r_T in the articl.
%% Hr is the yearly control mortality, denoted beta in the article 
%% The last parameters are for the boundary counditions


Res = zeros(size(Data.Center,1) , size(Data.Center,2),T);
D = zeros(size(Data.Center,1) , size(Data.Center,2));

Dist = sqrt((Data.CoordC(:,1)-Data.Barycentre(1).*ones).^2+(Data.CoordC(:,2)-Data.Barycentre(2).*ones).^2)./1000;
D(Dist <= 0.58) = 100;

deltaX = 1;%(1/size(D,2))^2;
deltaY = 1;%(1/size(D,1))^2;
deltaT = 0.01;

eps = 0.1;
NbMax = 500;

DN = D;

itr = 1;
Tsum = sum(repmat(reshape(Efficacy',[1 , 1 , length(Efficacy)]),[size(pT,1) , size(pT,2) , 1]).*pT,3);
DiffCoef = Data.Diff_Coef./2;


for year = 1:T
    
    for t = 1:(1/deltaT)
        
        errorD = 100;
        itr = 1;
        while (errorD > eps) && (itr < NbMax)
            
            itr = itr + 1;
            %% n
            Dip1 = [D(:,2:end) , zeros(size(D,1),1)];
            Dim1 = [zeros(size(D,1),1) , D(:,1:(end-1))];
            
            Djp1 = [zeros(1,size(D,2)) ; D(1:(end-1),:)];
            Djm1 = [D(2:end,:) ; zeros(1,size(D,2))];
            
            B = D;
            
            B = B + (deltaT/deltaX).*(DiffCoef.*( Dip1 - 2.*D + Dim1 ));
            
            B = B + (deltaT/deltaY).*(DiffCoef.*( Djp1 - 2.*D + Djm1 ));
            
            Growth = Data.GrowthRate.*(ones - D./Data.Kcapacity);
            
            Growth = Growth - Tsum - Hr;
            
            
            B = B + (1/2).*(deltaT.*(D.*Growth));
            
            %% n + 1
            Dip1 = [DN(:,2:end) , zeros(size(D,1),1)];
            Dim1 = [zeros(size(D,1),1) , DN(:,1:(end-1))];
            
            Djp1 = [zeros(1,size(D,2)) ; DN(1:(end-1),:)];
            Djm1 = [DN(2:end,:) ; zeros(1,size(D,2))];
            
            BN = (deltaT/deltaX).*(DiffCoef.*( Dip1 - 2.*DN + Dim1 ));
            
            BN = BN + (deltaT/deltaY).*(DiffCoef.*( Djp1 - 2.*DN + Djm1 ));
            
            Growth = Data.GrowthRate.*(ones - DN./Data.Kcapacity);
            
            Growth = Growth - Tsum - Hr;
            
            
            BN = BN + (1/2).*(deltaT.*(DN.*Growth));
            
            
            
            DN = B + BN;
            DN(Data.ZNumber == 0) = zeros;
            DN(Data.ZNumber == 12) = zeros;
            DN(IG) = DN(IG2);
            DN(IL) = DN(IL2);
            DN(IN) = zeros;
            
            errorD = norm(D-DN,inf)/norm(DN,inf);
            
        end
        D = DN;
    end
    Res(:,:,year) = D;
end
% figure(3)
% [X , Y] = meshgrid(1:size(D,2),1:size(D,1));
% D(D>Data.Kcapacity) = Data.Kcapacity;
% mesh(Y,X,D)


