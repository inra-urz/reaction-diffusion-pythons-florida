%% Clear all vairables in the orkspace and clean command window
clear all
clc

%% load all the data, i.e. info about python start and bounddaries of the map for the RD model. 
load Data.mat


%% Define some model parameers
Data.Diff_Coef = 16.2;
Data.GrowthRate = 0.468;
Data.rm = 39.4;

%% Do the prediciton, with no control at all! 
M = Logistic_Density(41,Data,0,zeros(size(Data.Center,1),size(Data.Center,2),1),...
    zeros(size(Data.Center,1),size(Data.Center,2)),IG,IG2,IL,IL2,IN);


%%%% Save predictions
I = intersect(find(Data.ZNumber~=0),find(Data.ZNumber~=12));

%% Save prediction in UTM coordinates
y = 1995:2035;
fspec = './predictions/Prediction_UTM_%d.xlsx';
for i = 1:numel(y)
    
    D = squeeze(M(:,:,i));
    Z = table(Data.CoordC(I,1) + x0,Data.CoordC(I,2) + y0,D(I),'VariableNames',{'x-east','y-north','Predicted_Density'});
    writetable(Z,sprintf(fspec,y(i)))
end


%% Save prediction in Lat-Long
[lat , long] = utm2deg(Data.CoordC(I,1) + x0,Data.CoordC(I,2) + y0,repmat('17 N',[numel(I) , 1]));

fspec = './predictions/Prediction_LatLong_%d.xlsx';
for i = 1:numel(y)
    
    D = squeeze(M(:,:,i));
    Z = table(Data.CoordC(I,1) + x0,Data.CoordC(I,2) + y0,D(I),'VariableNames',{'x-east','y-north','Predicted_Density'});
    writetable(Z,sprintf(fspec,y(i)))
end




